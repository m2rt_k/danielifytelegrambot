package Database;

import org.telegram.telegrambots.logging.BotLogger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created on 4.03.2017.
 */
public class DBConnection {
    private static final String TAG = "DBCONNECTION";
    private Connection currentConnection;

    public DBConnection() {
        this.currentConnection = openConnection();
    }

    private Connection openConnection() {
        Connection connection = null;
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection("jdbc:h2:./imagesDB", "db", "");
        } catch (ClassNotFoundException | SQLException e) {
            BotLogger.error(TAG, e);
        }
        return connection;
    }

    public PreparedStatement getPreparedStatement(String query) throws SQLException {
        return this.currentConnection.prepareStatement(query);
    }

    public boolean execute(String query) throws SQLException {
        return this.currentConnection.createStatement().execute(query);
    }

    // TODO: 04.04.2017 Make this better
    public boolean tablesExist() {
        try {
            getPreparedStatement("SELECT * FROM IMAGES").execute();
            getPreparedStatement("SELECT * FROM TABLES").execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }
}
