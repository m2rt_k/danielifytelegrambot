package Database;

import org.telegram.telegrambots.logging.BotLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created on 4.03.2017.
 */
public class DatabaseManager {
    private static final String TAG = "DATABASEMANAGER";
    private static DBConnection connection;

    private DatabaseManager() {
        connection = new DBConnection();
        if (!connection.tablesExist()) {
            try {
                connection.execute("CREATE TABLE IF NOT EXISTS IMAGES (CHATID BIGINT, IMAGE BLOB NOT NULL);" +
                                   "CREATE TABLE IF NOT EXISTS UPLOAD (CHATID BIGINT IDENTITY)");
            } catch (SQLException e) {
                BotLogger.error(TAG, e);
            }
        }
    }

    private static class SingletonHelper {
        private static final DatabaseManager instance = new DatabaseManager();
    }

    public static DatabaseManager getInstance() {
        return SingletonHelper.instance;
    }

    public boolean addChatForUpload(long chatId) {
        int s = -1;
        try {
            final PreparedStatement preparedStatement = connection.getPreparedStatement("INSERT INTO UPLOAD(CHATID) VALUES(?)");
            preparedStatement.setLong(1, chatId);
            s = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            BotLogger.error(TAG, e);
        }
        return s > 0;
    }

    public boolean getChatForUpload(long chatId) {
        try {
            final PreparedStatement preparedStatement = connection.getPreparedStatement("SELECT CHATID FROM UPLOAD WHERE CHATID=?");
            preparedStatement.setLong(1, chatId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                resultSet.close();
                return true;
            }
        } catch (SQLException e) {
            BotLogger.error(TAG, e);
        }
        return false;
    }

    public boolean removeChatForUpload(long chatId) {
        int s = -1;
        try {
            final PreparedStatement preparedStatement = connection.getPreparedStatement("DELETE FROM UPLOAD WHERE CHATID=?");
            preparedStatement.setLong(1, chatId);
            s = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            BotLogger.error(TAG, e);
        }
        return s > 0;
    }

    public boolean uploadImage(long chatId, File file) {
        int s = -1;
        try {
            final PreparedStatement preparedStatement = connection.getPreparedStatement("INSERT INTO IMAGES(CHATID, IMAGE) VALUES(?,?)");

            FileInputStream fis = new FileInputStream(file);
            preparedStatement.setLong(1, chatId);
            preparedStatement.setBlob(2, fis);
            s = preparedStatement.executeUpdate();
        } catch (SQLException | FileNotFoundException e) {
            BotLogger.error(TAG, e);
        }
        return s > 0;
    }

    public List<InputStream> getAllImages(long chatId) {
        List<InputStream> images = new ArrayList<>();
        try {
            final PreparedStatement preparedStatement = connection.getPreparedStatement("SELECT IMAGE FROM IMAGES WHERE CHATID=?");
            preparedStatement.setLong(1, chatId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) images.add(resultSet.getBlob(1).getBinaryStream());

        } catch (SQLException e) {
            BotLogger.error(TAG, e);
        }
        return images;
    }

    public InputStream getRandomImage(long chatId) {
        List<InputStream> images = getAllImages(chatId);
        if (images.size() < 1) return null;
        return images.get(new Random().nextInt(images.size()));
    }

    public boolean clear(long chatId) {
        int s = -1;
        try {
            final PreparedStatement preparedStatement = connection.getPreparedStatement("DELETE FROM IMAGES WHERE CHATID=?");
            preparedStatement.setLong(1, chatId);
            s = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            BotLogger.error(TAG, e);
        }
        return s > 0;
    }
}
