package FaceDetection;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.telegram.telegrambots.logging.BotLogger;

import java.nio.file.Files;
import java.nio.file.Paths;

public class FaceDetector {
    private static final String TAG = "FACEDETECTOR";

    private CascadeClassifier cascadeClassifier;

    public FaceDetector() {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        this.cascadeClassifier = new CascadeClassifier(
                getClass().getResource("lbpcascade_frontalface.xml")
                        .getPath().substring(1).replaceAll("%20", " ")
        );
    }

    /**
     * Detects faces in image.
     * @param image image to detect faces in.
     * @return an array of rectangles which depict the location of faces in image.
     */
    public Rect[] detect(Mat image) {
        MatOfRect faceDetections = new MatOfRect();
        cascadeClassifier.detectMultiScale(image, faceDetections);
        return faceDetections.toArray();
    }

    /**
     * Replaces faces in image with randomly chosen images from replacement list.
     * Writes "result.png" file to project root.
     * @param image image to replace faces in.
     * @param faceLocs rectangles of face locations in image.
     * @param replacement replacement image.
     */
    public void replace(Mat image, Rect[] faceLocs, Mat replacement) {
        for (Rect rect : faceLocs)
            copyImageToImage(image, replacement, rect);

        Imgcodecs.imwrite("result.png", image);
    }

    /**
     * Outlines faces in image.
     * Writes "result.png" file to project root.
     * @param image image to outline faces in.
     * @param faceLocs rectangles of face locations in image.
     */
    public void outline(Mat image, Rect[] faceLocs) {
        for (Rect rect : faceLocs)
            Imgproc.rectangle(image,
                    new Point(rect.x, rect.y),
                    new Point(rect.x+rect.width, rect.y+rect.height),
                    new Scalar(0, 255, 0)
            );
        Imgcodecs.imwrite("result.png", image);
    }

    /**
     * Deletes "result.png" file from project root.
     */
    public void clear() {
        try {
            Files.delete(Paths.get("result.png"));
        } catch (Exception e) {
            BotLogger.error(TAG, e);
        }
    }

    /**
     * Copies one image onto another pixel by pixel, ignoring pixels which alpha value is 0.
     * If image sizes differ, resizes the from image to the same size as the to image.
     * @param to image to copy to.
     * @param from image which to copy.
     * @param location location to copy to.
     */
    private void copyImageToImage(Mat to, Mat from, Rect location) {
        if (to.cols() != from.cols() || to.rows() != from.rows()) {
            Mat smallerFrom = new Mat();
            Imgproc.resize(from, smallerFrom, new Size(location.width, location.height));
            from = smallerFrom;
        }
        if (from.channels() > 3) {
            double[] s;
            for (int y = 0; y < from.height(); y++)
                for (int x = 0; x < from.width(); x++) {
                    s = from.get(y, x);
                    if (from.get(y, x)[0] > 0)
                        to.put(location.y + y, location.x + x, s[1], s[2], s[3]);
                }
        } else {
            double[] s;
            for (int y = 0; y < from.height(); y++)
                for (int x = 0; x < from.width(); x++) {
                    s = from.get(y, x);
                    to.put(location.y + y, location.x + x, s[0], s[1], s[2]);
                }
        }
    }
}
