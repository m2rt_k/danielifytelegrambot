package FaceDetection;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.telegram.telegrambots.logging.BotLogger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    private static Mat bufferedImageToMat(BufferedImage bi) {
        int cvType = bi.getColorModel().hasAlpha() ? CvType.CV_8UC4 : CvType.CV_8UC3;
        Mat mat = new Mat(bi.getHeight(), bi.getWidth(),  cvType);
        byte[] data = ((DataBufferByte) bi.getRaster().getDataBuffer()).getData();
        mat.put(0, 0, data);
        return mat;
    }

    public static Mat imageToMat(String path) {
        return Imgcodecs.imread(path, Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
    }

    public static Mat inputStreamToMat(InputStream is) throws IOException {
        return bufferedImageToMat(ImageIO.read(is));
    }
}
