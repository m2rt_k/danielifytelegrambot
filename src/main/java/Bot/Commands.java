package Bot;

public class Commands {
    public static final String initChar = "/";

    public static final String help = initChar + "help";

    public static final String hello = initChar + "hello";

    public static final String upload = initChar + "upload";

    public static final String cancel = initChar + "cancel";

    public static final String clear = initChar + "clear";

    public static final String nice = initChar + "nice";
}
