package Bot;

import Database.DatabaseManager;
import FaceDetection.FaceDetector;
import FaceDetection.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.telegram.telegrambots.api.methods.GetFile;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Document;
import org.telegram.telegrambots.api.objects.PhotoSize;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class DanielifyBot extends TelegramLongPollingBot {
    private static final String TAG = "DANIELIFYBOT";
    private static final FaceDetector fd = new FaceDetector();

    public void onUpdateReceived(Update update) {
        BotLogger.info(TAG, "onUpdateReceived called");

        if (!update.hasMessage()) {
            BotLogger.info(TAG, "No message, Update end");
            return;
        } else {
            BotLogger.info(TAG, "Update from chat: " + update.getMessage().getChatId());
        }

        if (update.getMessage().isCommand()) {
            String[] parts = update.getMessage().getText().split(" ", 2);

            if (parts[0].startsWith(Commands.hello)) onHelloCommand(update);
            else if (parts[0].startsWith(Commands.help)) onHelpCommand(update);
            else if (parts[0].startsWith(Commands.upload)) onUploadCommand(update);
            else if (parts[0].startsWith(Commands.cancel)) onCancelCommand(update);
            else if (parts[0].startsWith(Commands.clear)) onClearCommand(update);
            else if (parts[0].startsWith(Commands.nice)) onNiceCommand(update);
        } else if (DatabaseManager.getInstance().getChatForUpload(update.getMessage().getChatId())) {
            if (update.getMessage().hasDocument() || update.getMessage().hasPhoto()) handleUpload(update);
        } else if (update.getMessage().hasPhoto()) {
            handlePhoto(update);
        }

        BotLogger.info(TAG, "Update end");
    }


    private void onNiceCommand(Update update) {
        sendMessage("nice", update.getMessage().getChatId());
    }


    private void onClearCommand(Update update) {
        DatabaseManager.getInstance().clear(update.getMessage().getChatId());
        sendMessage("All images cleared!", update.getMessage().getChatId());
    }

    private void onCancelCommand(Update update) {
        DatabaseManager.getInstance().removeChatForUpload(update.getMessage().getChatId());
        sendMessage("Upload canceled!", update.getMessage().getChatId());
    }

    private void onUploadCommand(Update update) {
        DatabaseManager.getInstance().addChatForUpload(update.getMessage().getChatId());
        sendMessage("Please upload an image now! Use /cancel to cancel this.", update.getMessage().getChatId());
    }

    private void onHelpCommand(Update update) {
        StringBuilder sb = new StringBuilder();
        sb.append("Amount of uploaded images currently in database: ");
        sb.append(DatabaseManager.getInstance().getAllImages(update.getMessage().getChatId()).size());
        sb.append(System.getProperty("line.separator"));
        sb.append("Send more images using /upload command.");
        sb.append(System.getProperty("line.separator"));
        sb.append("Use /clear command to remove all images in database.");
        sb.append(System.getProperty("line.separator"));
        sb.append("Send a image in this chat to replace faces in image with images in database.");
        sendMessage(sb.toString(), update.getMessage().getChatId());
    }

    private void onHelloCommand(Update update) {
        String[] parts = update.getMessage().getText().split(" ", 2);
        StringBuilder sb = new StringBuilder();
        sb.append("Hello, ");
        sb.append(update.getMessage().getFrom().getFirstName());
        sb.append("!");

        if (parts.length > 1 && !parts[1].isEmpty()) {
            sb.append(" Thanks for the kind words!");
        }
        sendMessage(sb.toString(), update.getMessage().getChatId());
    }

    private void handleUpload(Update update) {
        String path = null;
        if (update.hasMessage() && update.getMessage().hasPhoto()) {
            path = getFilePath(getPhoto(update));
        } else if (update.hasMessage() && update.getMessage().hasDocument()) {
            path = getFilePath(getDocument(update));
        }

        if (path != null) {
            File file = downloadByFilePath(path);
            if (file != null) {
                if (DatabaseManager.getInstance().uploadImage(update.getMessage().getChatId(), file))
                    sendMessage("Image successfully uploaded", update.getMessage().getChatId());
                else sendMessage("Something went wrong with upload, feel free to try again.", update.getMessage().getChatId());

                DatabaseManager.getInstance().removeChatForUpload(update.getMessage().getChatId());
            }
        }
    }

    private void handlePhoto(Update update) {
        PhotoSize image = getPhoto(update);
        if (image != null) {
            String path = getFilePath(image);
            if (path != null) {
                File file = downloadByFilePath(path);
                if (file != null) {
                    Rect[] faces = fd.detect(Utils.imageToMat(file.getPath()));
                    if (faces.length != 0) {
                        BotLogger.info(TAG, "Detected " + faces.length + " faces.");

                        InputStream is = DatabaseManager.getInstance().getRandomImage(update.getMessage().getChatId());
                        Mat face = null;
                        if (is != null) {
                            try {
                                face = Utils.inputStreamToMat(is);
                            } catch (IOException e) {
                                BotLogger.error(TAG, e);
                            }
                        }

                        if (face != null) fd.replace(Utils.imageToMat(file.getPath()), faces, face);
                        else fd.outline(Utils.imageToMat(file.getPath()), faces);


                        sendImageUploadingAFile("result.png", String.valueOf(update.getMessage().getChatId()));
                        fd.clear();
                    } else {
                        BotLogger.info(TAG, "No faces found.");
                    }
                }
            }
        }
    }

    private void sendMessage(String message, Long chatId) {
        try {
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(chatId);
            sendMessage.setText(message);
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            BotLogger.error(TAG, e);
        }
    }

    private PhotoSize getPhoto(Update update) {
        if (update.hasMessage() && update.getMessage().hasPhoto()) {
            // When receiving a photo, you usually get different sizes of it
            List<PhotoSize> photos = update.getMessage().getPhoto();

            // We fetch the bigger photo
            return photos.stream()
                    .sorted(Comparator.comparing(PhotoSize::getFileSize).reversed())
                    .findFirst()
                    .orElse(null);
        }
        return null;
    }

    private Document getDocument(Update update) {
        if (update.hasMessage() && update.getMessage().hasDocument()) {
            return update.getMessage().getDocument();
        }
        return null;
    }

    private String getFilePath(Document document) {
        Objects.requireNonNull(document);

        GetFile getFileMethod = new GetFile();
        getFileMethod.setFileId(document.getFileId());
        try {
            org.telegram.telegrambots.api.objects.File file = getFile(getFileMethod);
            return file.getFilePath();
        } catch (TelegramApiException e) {
            BotLogger.error(TAG, e);
        }

        return null;
    }

    private String getFilePath(PhotoSize photo) {
        Objects.requireNonNull(photo);

        if (photo.hasFilePath()) {
            return photo.getFilePath();
        } else {
            GetFile getFileMethod = new GetFile();
            getFileMethod.setFileId(photo.getFileId());
            try {
                org.telegram.telegrambots.api.objects.File file = getFile(getFileMethod);
                return file.getFilePath();
            } catch (TelegramApiException e) {
                BotLogger.error(TAG, e);
            }
        }

        return null;
    }

    private File downloadByFilePath(String filePath) {
        try {
            return downloadFile(filePath);
        } catch (TelegramApiException e) {
            BotLogger.error(TAG, e);
        }
        return null;
    }

    private void sendImageUploadingAFile(String filePath, String chatId) {
        SendPhoto sendPhotoRequest = new SendPhoto();
        sendPhotoRequest.setChatId(chatId);
        sendPhotoRequest.setNewPhoto(new File(filePath));
        try {
            sendPhoto(sendPhotoRequest);
        } catch (TelegramApiException e) {
            BotLogger.error(TAG, e);
        }
    }

    public String getBotUsername() {
        return Config.DANIELIFY_USER;
    }

    public String getBotToken() {
        return Config.DANIELIFY_TOKEN;
    }

}
